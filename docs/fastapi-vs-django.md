# FastAPI vs Django

## Django?
Django는 전적으로 Python을 기반으로 구축된 웹 프레임워크이다. 무료 오픈소스 프레임워크이다. 웹 프레임워크는 동적 웹사이트를 개발하는 데 도움이 되도록 설계된 서버 측(side) 어플리케이션 프레임워크이다. 이러한 프레임워크는 웹 개발 및 구성 요소와 관련된 번거로움에 대해 걱정할 필요가 없기 때문에 개발을 쉽게 만들어 준다. Django는 Python을 기반으로 구축되었기 때문에 DRY(Don't Reapeat Yourself) 원칙을 따른다. 이는 코드를 단순하고 반복되지 않게 유지하는 데 도움이 된다.

이미 많은 대기업이 Django 프레임워크를 사용하고 있다. 예를 들어 Instagram, Pinterest, Mozilla, Disqus 등이 있다.

## Django의 기능:
Django는 개발자가 웹 어플리케이션 프로젝트를 빠르게 개발할 수 있도록 도와주는 훌륭한 오픈소스 프레임워크이다. 하지만 이 외에도 무시할 수 없는 Django만의 장점과 기능이 있다.

- 크로스 사이트 스크립팅(cross-site scripting), SQL 인젝션(SQL injection) 등과 같은 일반적인 위협을 방지하는 우수한 보안 기능을 제공한다.
- 계정과 비밀번호를 관리할 수 있는 사용자 인증 시스템을 제공한다.
- Django 프로젝트는 소규모 프로젝트에서 대규모 프로젝트로 빠르게 전환할 수 있다.
- 또한 Django는 개발자가 다양한 타입의 어플리케이션을 구축할 수 있도록 유연성을 제공하는 다목적 프레임워크라고 할 수 있다.
- Django는 개발자가 작업하는 동안 도움을 주는 방대한 커뮤니티가 있어 웹 프레임워크 중에서 인기가 높다.

Django에 대한 문서는 [여기](https://docs.djangoproject.com/en/4.0/)에서 참조할 수 있다.

## Django 구조:
Django는 모델 뷰 컨트롤러(Model View Controller) 아키텍처를 기반으로 하는 모델 뷰 템플릿(Model View Template)를 기반인 아키텍처이다. 그렇다면 이 MVT와 MVC는 무엇일까? 자세히 살펴보자.

1. **모델**: 데이터베이스를 정의하는 백엔드이다. 데이터베이스 위에 CRUD(Create, Read, Update, Delete) 객체를 생성하여 데이터를 처리하는 데 도움을 준다. 데이터를 저장하고 유지 관리하는 데 사용된다.
1. **보기**: 보기는 모델에서 데이터를 가져오는 데 사용된다. 비즈니스 로직을 실행하고 모델에서 템플릿으로 데이터를 전달하여 작절히 렌더링한다. 또한 HTTP 요청을 수락하고 클라이언트 요청에 대한 HTTP 응답을 제공한다.
1. **템플릿**: 사용자 인터페이스 부분을 처리하므로 프레젠테이션 레이어라고도 한다. 템플릿은 데이터를 출력하는 데 사용되는 HTML 데이터가 포함된 파일이다. 이러한 템플릿 파일의 콘텐츠는 정적이거나 동적일 수 있다.

아래 다이어그램으로 그 구조를 설명할 수 있다.

![](./0_66SzqLthEtm2xkOq.webp)

이 아키텍처에서 템플릿은 뷰와 상호 작용하는 프런트엔드이다. 또한 백엔드로 사용되는 모델과도 상호 작용한다. 그런 다음 뷰는 모델과 템플릿을 모두 액세스한 다음 특정 URL에 매핑된다. 그 후 컨트롤러로서 Django가 사용자에게 서비스를 제공한다.

앱을 만들고 실행하려면 다음 단계를 따라야 한다.

1. Django 설치

```bash
$ pip install django
```

2. 버전 확인

```bash
$ python -m django — version
```

3. 사용 가능한 django 명령 확인

```bash
$ django-admin
```

4. 프로젝트 생성

```bash
$ django-admin startproject hello_world_app
```

5. 이제 프로젝트 디렉토리로 이동

```bash
$ cd hello_world_app
```

6. 기본 Django 홈페이지가 보일 수 있도록 서버를 실행한다.

```bash
$ python manage.py runserver
```

7. 이제 현재 디렉토리(hello_world_app)에서 웹 앱을 만들 수 있다. 앱을 만들려면 다음 명령을 사용한다.

```bash
$ python manage.py startapp APP_NAME
```

지금까지 Django에 대한 간략한 소개를 마쳤다. 이제 FastAPI에 대해 알아보자!

## FastAPI?
FastAPI는 REST 서비스를 구축하는 데 가장 많이 사용되는 Python 프레임워크 중 하나이기도 하다. 사용하기 매우 쉽고, Django보다 훨씬 간단하며, 배포하기 쉽다. FastAPI는 Django의 많은 단점을 보완한다. FastAPI는 웹 개발을 위한 최신 고성능 웹 프레임워크이며, Python 3.6 이상 버전에서만 사용이 가능하다. FastAPI는 그 이름에서 충분히 알 수 있듯이 매우 빠르다. FastAPI로 개발을 시작하려면 먼저 FastAPI와 **uvicorn**을 설치하여야 한다. 이 두 가지 모두 `pip`를 사용하여 설치할 수 있다.

uvicorn은 무엇일까? 프로덕션에 사용되는 비동기 서버 게이트웨이 인터페이스(ASGI) 서버이다. FastAPI는 개발자 요청을 비동기적으로 처리할 수 있는 이점을 제공한다.

[FastAPI 설명서](https://fastapi.tiangolo.com/)를 참조하세요.

## FastAPI의 기능:
FastAPI는 API 개발과 웹 개발을 위한 Python 프레임워크들 중 가장 빠른 것 중 하나이다. 또한 다음과 같은 추가적인 이점을 갖고 있다.

- FastAPI의 문서는 대화형이며, 간단하고 직관적이며, 훌륭한 편집기 지원을 제공한다.
- 코드 수행 속도가 빠르므로 기능 개발 속도가 약 200~300% 향상된다.
- FastAPI는 명령형 코딩 스타일을 제공하여 버그 유발의 약 40%를 줄였다고 한다.
- 또한 API와 JSON 스키마에 대한 개방형 표준과 호환된다.

## 설치:

```bash
$ pip install fastapi
$ pip install uvicorn
```

FastAPI의 가장 큰 장점은 단 5줄의 코드만으로 웹사이트의 첫 번째 엔드포인트를 시작할 수 있다는 것이다.

예:

```python
from fastapi import FastAPI


app = FastAPI()

@app.get("/")
def read_root():
    return {"message": "Hello Aliens!!"}
```

이 코드를 `app.py`에 저장하고 다음 명령을 실행한다.

```bash
$ uvicorn app:app --reload
``` 

출력:

![](./0_nk6pbcH-zfmx2zl-.webp)

FastAPI에서 미리 정의된 HTTP의 4가지 메서드를 사용하여 데이터베이스에서 데이터를 생성, 읽기, 업데이트 및 삭제할 수 있다.

- @app.get()
- @app.post()
- @app.put()
- @app.delete()

이제 이 두 가지 놀라운 프레임워크 간의 전투를 시작해 보겠다.

## Django와 FastAPI 차이점
아시다시피, Django는 웹 어플리케이션을 구축하기 위한 거대한 프레임워크인 반면, FastAPI는 비동기 IO를 사용하여 빠른 웹 어플리케이션을 구축하기 위한 Starlette 과 Pydantic 기반의 미니멀리즘 프레임워크이다.

Django는 2005년에 등장한 꽤 오래된 언어인 반면, FastAPI는 2018년에 등장한 훨씬 새로운 언어이며 Python 3.6 이상 버전만 지원한다. 이 때문에 Django는 FastAPI에 비해 더 큰 지원 커뮤니티를 보유하고 있다. 따라서 Django에서 문제가 발생할 때마다 아주 쉽게 도움을 받을 수 있지만, FastAPI에서는 약간의 어려움을 겪을 수 있다.

- **성능**: 최소한의 프레임워크인 FastAPI는 Django에 비해 가볍기 때문에 매우 높은 성능을 제공하는 반면, Django는 마이크로 프레임워크인 FastAPI에 비해 빠르지 않다.
- **학습 곡선**: FastAPI는 배우기 매우 쉬우며 웹 개발을 처음 접하는 사람들에게 유용하다. 반면, Django는 초보자에게는 조금 어려울 수 있지만 온라인에 많은 강좌, 튜토리얼 등이 있어 웹 개발 분야의 초보자도 쉽게 배울 수 있다.
- **유연성**: FastAPI는 개발자를 특정 코드 레이아웃으로 제한하지 않으므로 코딩하는 동안 자유로운 느낌을 준다. 하지만 DJango는 이 프레임워크처럼 작동하지 않는다. 코딩 표준을 유하고 특정 방식으로 작업이 수행되기를 기대한다.
- **문서화**: Django는 매우 훌륭한 문서 지원을 제공하지만 대화형 문서가 아닌 반면, FastAPI는 대화형 문서를 제공한다. 예를 살펴보자.

Code:

```python
from fastapi import FastAPI


app = FastAPI()

@app.get("/")
def read_root():
    return {"message": "Hello Aliens!!"}

@app.get("/items/{item_id}")
def read_item(item_id: int, q: str = None):
    return {"item_id": item_id, "q": q}
```

이 코드를 실행하고 http://127.0.0.1:8000/docs 으로 이동한다.

Outpit:

대화형 API 문서가 자동으로 생성되어 브라우저 출력으로 확인할 수 있다.

![](./0_CYRw4dMg77aTMy8x.webp)

엔드포인트의 드롭다운 중 하나를 클릭하여 이러한 API를 사용해 볼 수 있다. 위 목록에서 두 번째 API를 클릭하면 아래와 같이 확장되고 이제 필요한 파라미터를 사용하여 API를 테스트할 수 있다.

![](./0_mKwGA41dC3BXooJS.webp)

필드에 매개변수를 전달하면 API 실행에 대한 응답을 얻을 수 있다.

FastAPI는 대화형 API 문서 유형을 하나 더 제공한다. 이를 보려면, URL http://127.0.0.1:8000/redoc 를 방문한다.

![](./0_RfyK_f1z3mY-atru.webp)

대단하지 않나요? 이렇게 쉬운 대화형 문서를 제공하고 번거로움 없이 API를 테스트할 수 있기 때문에 개발자의 삶이 훨씬 쉬워졌다.

**NoSQL 데이터베이스 지원**: FastAPI는 ElasticSearch, MongoDB 등과 같은 많은 NoSQL 데이터베이스를 지원한다. Django에서는 NoSQL 데이터베이스를 공식적으로 지원하지 않으므로 Django와 함께 사용하지 않는 것이 좋다.

**보안**: Django는 CSRF, SQL 인젝션 등과 같은 일반적인 공격을 피할 수 있는 매우 우수한 보안 기능을 갖추고 있다. FastAPI 또한 보안을 위해 모듈(fastapi.security)에 여러 가지 도구를 제공한다. 하지만 여전히 Django는 FastAPI보다 훨씬 더 안전한 것으로 여겨지고 있다.

**REST API 지원**: FastAPI를 사용하면 개발자가 REST API를 빠르게 빌드할 수 있다. 위의 예에서도 단 5줄의 코드만으로 하나의 API가 준비되었다. 반면에 Django에는 REST API 지원이 포함되어 있지 않다. 하지만 Django REST 프레임워크에서 지원된다.

## 선택은?
이 싸움은 여기서 끝내자!

Django는 내부적으로 엄청난 힘을 제공하는 본격적인 웹 프레임워크이다. 하지만 초보 웹 개발자에게는 다소 복잡하고 문제를 일으킬 수 있다. 반면에 FastAPI는 매우 간단하고 가볍고 사용하기 쉬운 마이크로 프레임워크로서 필요한 기능만 제공한다. 따라서 Django는 매우 견고하지만 FastAPI는 사용하기 쉽다. 이제 여러분의 편의와 작업 중인 사례에 따라 어떤 프레임워크를 선택할지 결정할 수 있다. 물론 두 프레임워크 모두 훌륭하니 모두 사용해 보기를 추천한다.
